#  API tests

Tests the below points
--
    Every response should return 200 OK.
    First node should have more than 10 categories on children_data node.
    Every node should have an "id"
    Every node should have a "name"
    Every node should have a “url”
    Every node should have an “image” except 3rd level category
    Third level category should have “includeInMenu” as false
    Last level of categories should have “availableSortBy” minimum of 2 sub nodes;
     1.shoeSizeCode
     2. designer
    A node with featureLink with “/edits/*” should be in 3rd leve
