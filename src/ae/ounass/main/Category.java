package ae.ounass.main;

import java.util.List;

public class Category {
	public int categoryId;
	public String key;
	public String name;
	public boolean includeInMenu;
	public int level;
	public List<Category> children;
	public String displayMode;
	public String[] availableSortBy;
	public String fontColor;
	public String url;
	public String image;
	public String featureLink;
	public int parentCategoryId;
	public String description;
	public String featureImage;
	public String featureLinkText;
	public String frontendClass;
	public String aliasUrl;
	public int excludeFromRecommendations;
	public String contentBlockLink;
	public String contentBlockImage;
}