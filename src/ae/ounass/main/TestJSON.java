package ae.ounass.main;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import com.google.gson.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.net.ssl.HttpsURLConnection;

/**
 * @author Vanitha
 * Helper class to read data from API and filter it .
  */
public class TestJSON {

	private String jsonValues;
	static private Category data;
	static List<Category> lstCategory = new ArrayList<Category>();

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public int getResponse(String urlString) throws IOException {
		URL url = new URL(urlString);
		HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("user-agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
		connection.connect();
		InputStream is = connection.getInputStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			jsonValues = readAll(rd);
			data = new Gson().fromJson(jsonValues, Category.class);

		} finally {
			is.close();
		}
		return connection.getResponseCode();
	}

	public int getAllChildNode() {
		return data.children.size();
	}

	/**
	 * Flatten list so recursion need not to be done each time . Children inside wont be taken into account since they are already in main list . 
     */
	public void FlattenList() {
		// Add parent category also as entry
		lstCategory.add(data);
		RecurseToGetCaterogies(data.children);
		lstCategory.size();	
	}

	public static void RecurseToGetCaterogies(List<Category> Children) {
		for (Category currentChild : Children) {
				lstCategory.add(currentChild);
				RecurseToGetCaterogies(currentChild.children);
		}
}

	public List<Integer> getNodeLevelsWithFeatureLinkEdit() {
		List<Integer> levels = new ArrayList<Integer>();
		for (Category currentChild : lstCategory) {
			if (currentChild.featureLink != null && currentChild.featureLink.startsWith("/edits/")) {
				if (!levels.contains(currentChild.level)) {
					levels.add(currentChild.level);
				}
			}
		}
		return levels;
	}

	public static <T> List<T> search(Collection<T> collectionObject, Function<T, String> searchPropertyAccessor,
			String searchText) {

		List<T> result = collectionObject.stream()
				.filter(item -> Objects.equals(searchPropertyAccessor.apply(item), searchText))
				.collect(Collectors.toList());
		return result;
	}

	public Integer findLastLevelFromNodes() {
		int lstLevel = 0;
		for (Category currentChild : lstCategory) {
			if (currentChild.level > lstLevel) {
				lstLevel = currentChild.level;
			}
		}
		return lstLevel;
	}

	public List<String[]> getAvailableSortByGivenLevel(int level) {
		List<String[]> availableSortByInGivenLevel = new ArrayList<String[]>();
		for (Category currentChild : lstCategory) {
			if (currentChild.level == level) {
				availableSortByInGivenLevel.add(currentChild.availableSortBy);
			}
		}
		return availableSortByInGivenLevel;
	}

	public Boolean validateIncludeMenuInGivenLevelToBeFalse(int level) {
		for (Category currentChild : lstCategory) {
			if (currentChild.level == level) {
				if (currentChild.includeInMenu == true)
					return false;
			}
		}
		return true;
	}

	public List<Integer> getIDFromAllNodes() {
		List<Integer> ids = new ArrayList<Integer>();
		for (Category currentChild : lstCategory) {
			ids.add(currentChild.categoryId);
		}
		return ids;
	}

	public List<String> getNameFromAllNodes() {
		List<String> names = new ArrayList<String>();
		for (Category currentChild : lstCategory) {
			names.add(currentChild.name);
		}
		return names;
	}

	public List<String> getUrlFromAllNodes() {
		List<String> urls = new ArrayList<String>();
		for (Category currentChild : lstCategory) {
			urls.add(currentChild.url);
		}
		return urls;
	}

	public List<String> getImageForGivenLevel(int level) {
		List<String> images = new ArrayList<String>();
		for (Category currentChild : lstCategory) {
			if (currentChild.level == level) {

				images.add(currentChild.image);
			}
		}
		return images;
	}

}
