package ae.ounass.test;

import java.util.List;
import java.io.IOException;
import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.Test;

import ae.ounass.main.TestJSON;

public class CategoryTest {

	public static TestJSON testJson;

	@Test
	public void checkStatus() throws IOException {
		testJson = new TestJSON();
		Assert.assertEquals(testJson.getResponse("https://www.ounass.ae/categories/getcategorytree/"), 200);
		try {
			testJson.FlattenList();
		} catch (Exception ex) {
			Assert.assertTrue(false, "Failed to convert JSON to Object");
		}

	}

	@Test(description = "First node should have more than 10 categories on children_data node.", dependsOnMethods = {
			"checkStatus" })
	public void validateChildCountOnFirstNode() {
		boolean result = (testJson.getAllChildNode() > 10) ? true : false;
		Assert.assertTrue(result);
	}

	@Test(description = "node with featureLink with “/edits/*” should be in 3rd level", dependsOnMethods = {
			"validateChildCountOnFirstNode" })
	public void validateFeatureLinkEditAreIn3rdLevel() {
		List<Integer> levels = testJson.getNodeLevelsWithFeatureLinkEdit();
		for (int level : levels) {
			Assert.assertTrue(level == 3);
		}
	}

	@Test(description = "Last level of categories should have “availableSortBy” minimum of 2 sub nodes", dependsOnMethods = {
			"validateChildCountOnFirstNode" })
	public void validateAvailableSortByInLastLevel() {

		int lastLevel = testJson.findLastLevelFromNodes();
		List<String[]> availableSort = testJson.getAvailableSortByGivenLevel(lastLevel);
		for (String[] availableType : availableSort) {
			List<String> values = Arrays.asList(availableType);
			Assert.assertTrue(values.size() >= 2, "Expected length to be > 2  but was " + values.size());
			Assert.assertTrue(values.contains("shoeSizeCode"), "Should contain shoeSizeCode ");
			Assert.assertTrue(values.contains("designer"), "Should contain designer");

		}
	}

	@Test(description = "Third level category should have “includeInMenu” as false", dependsOnMethods = {
			"validateChildCountOnFirstNode" })
	public void validateIncludeInMenu() {
		Assert.assertTrue(testJson.validateIncludeMenuInGivenLevelToBeFalse(3));
	}

	@Test(description = "Every node should have an “image” except 3rd level category", dependsOnMethods = {
			"validateChildCountOnFirstNode" })
	public void validateImage() {
		int lastLevel = testJson.findLastLevelFromNodes();
		for (int i = 0; i < lastLevel; i++) {
			List<String> imagesInNode = testJson.getImageForGivenLevel(i);
			if (i != 3) {
				Assert.assertTrue(!imagesInNode.contains(null), "Expected all node to have a image. Found node without image in node"+i);
			} else {
				for (String item : imagesInNode) {
					Assert.assertTrue(item == null || item.trim().isEmpty(),
							"Expected all node of level 3 to not have a image");
				}
			}
		}
	}

	@Test(description = "Every node should have an \"id\"", dependsOnMethods = { "validateChildCountOnFirstNode" })
	public void validateAllNodesHasID() {

		List<Integer> ids = testJson.getIDFromAllNodes();
		Assert.assertTrue(ids.size() > 0);
		Assert.assertFalse(ids.contains(0));
	}

	@Test(description = "Every node should have an \"name\"", dependsOnMethods = { "validateChildCountOnFirstNode" })
	public void validateAllNodesHasName() {

		List<String> names = testJson.getNameFromAllNodes();
		for (int i = 0; i < names.size(); i++) {
			Assert.assertTrue(names.get(i) != null && !names.get(i).trim().isEmpty());
		}
	}

	@Test(description = "Every node should have an \"URL\"", dependsOnMethods = { "validateChildCountOnFirstNode" })
	public void validateAllNodesHasURL() {
		List<String> urlS = testJson.getUrlFromAllNodes();

		for (int i = 0; i < urlS.size(); i++) {
			Assert.assertTrue(urlS.get(i) != null && !urlS.get(i).trim().isEmpty());
		}
	}
}
